class Camara(object):

    id_camara = None,
    longitud = None,
    latitud = None, 
    tipo = None,
    direccion = None,
    altura = None, 
    km = None,
    velocidad = None,
    barrio = None,
    comuna = None,
    codigo_postal = None

    def __init__(self,id,long,lat,tipo,dir,altura,km,vel,bar,comu,cod):
        self.id_camara = id
        self.longitud = long
        self.latitud = lat
        self.tipo = tipo
        self.direccion = dir
        self.altura = altura
        self.km = km
        self.velocidad = vel
        self.barrio = bar
        self.comuna = comu
        self.codigo_postal = cod


