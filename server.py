import csv
from datetime import datetime

from flask import Flask, stream_with_context
import io
from flask import Flask, request, make_response, redirect, render_template
from flask_script import Manager

from camara import Camara
from buenos_aires import BuenosAires
import json
from DB import DB
app = Flask(__name__)

app = Flask(__name__, static_url_path='/static')
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'


#CORS(app)
manager = Manager(app)
DB().setconnection("localhost","root","alumnoipm","controlvehicular")

BsAs = BuenosAires()

BsAs.resetear()

with open('camaras.csv', encoding="utf8") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
                cam = Camara(row["id"],row["long"],row["lat"],row["tipo"],row["calle_auto"],row["altura"],row["km"],row["velocidad"],row["barrio"],row["comuna"],row["codigo_postal"])
                BsAs.agregar_camara(cam)#Agregando el objeto camara a la lista

BsAs.agregar_camara_a_base()#Agrega objeto camara a la BD

@app.route('/', methods=['GET'])
def index():
        return redirect('/home')

@app.route('/home', methods=['GET'])
def home():
        return render_template('home.html', listaCamaras= BsAs.getListaCamaras())

@app.route('/camaras', methods=['GET', 'POST'])
def camara():

        pito = DB().run("select (select count(*) from camara where velocidad != 0) as CamarasVelocidad ,(select count(*) from camara where velocidad = 0) as CamarasNoVelocidad from camara limit 1")
        cantCam = pito.fetchall()
        print(cantCam)

        return render_template('graphSpeed.html', velocidad= cantCam[0]["CamarasVelocidad"], noVelocidad=cantCam[0]["CamarasNoVelocidad"])

@app.route('/barrio', methods= ['GET','POST'])
def barrio():

        return render_template('camaraPorBarrio.html')

@app.route('/barrioGetCsv', methods=['GET', 'POST'])
def returncsv():

        consulta = DB().run("select barrio as city, count(*) as snow from camara group by barrio")
        jsonBarrio = consulta.fetchall()
        
        si = io.StringIO()
        cw = csv.writer(si)
        cw.writerow(['city' , 'value'])
        for item in jsonBarrio:
                print(item)
                cw.writerow([item['city'], item['snow']])
        output = make_response(si.getvalue())
        output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        output.headers["Content-type"] = "csv"
        return output

@app.route('/comuna', methods=['GET','POST'])
def comuna():

        consulta = DB().run("select comuna as label, count(*) as value from camara group by comuna order by comuna")
        comuna = consulta.fetchall()
        print(comuna)

        return render_template('camarasPorComuna.html', camaraComunas = comuna)


@app.route('/velocidad', methods=['GET','POST'])
def velocidad():
        consulta= DB().run("select velocidad, count(*) as cantidad from camara group by velocidad having velocidad > 0;")
        lista = consulta.fetchall()

        return render_template('velocidad.html', velocidad = lista)

if __name__ == '__main__':
    manager.run()
    app.run(debug=True)