from camara import Camara
from DB import DB

class BuenosAires(object):

    lista_camaras = []

    def agregar_camara(self, camara):
        self.lista_camaras.append(camara)

    def agregar_camara_a_base(self):

        for item in self.lista_camaras:#Recorre la lista de camara 
            #Cada camara se inserta a la base
            DB().run("insert into camara (id,longitud,latitud,tipo,direccion,altura,km,velocidad,barrio,comuna,codigo_postal) values ('"+item.id_camara+"','"+item.longitud+"','"+item.latitud+"','"+str(item.tipo)+"','"+str(item.direccion)+"','"+item.altura+"','"+item.km+"','"+item.velocidad+"','"+str(item.barrio)+"','"+str(item.comuna)+"','"+item.codigo_postal+"');")
    
    def resetear(self):
        
        DB().run("truncate table camara;")
    

    def getListaCamaras(self):
        return self.lista_camaras 
